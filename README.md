# RD-TA-Gold-Certificate-Assessments


# Scenarios

1. Login Validation:

a. POST: Make an API call https://demoqa.com/Account/v1/User and create user login
using the below request body.
Request body: {
"userName": "<passtestdatauser>",
"password": "<passtestdatapwd>"
}
i. Validate status code.
ii. Validate response body.

b. WEB -UI: Login Validation
i. Go to url “https://demoqa.com/login”
ii. Enter username and password {created from the previous API call}.
iii. Click on the Login button.
iv. Validate Username on the Books Dashboard page.

2. Validate the book details.
   a. GET : Make an API call https://demoqa.com/BookStore/v1/Books and capture the
   details of all the books.
   b. Validate the captured details i.e., Title, Author, Publisher from above response by
   launching the URL https://demoqa.com/books on WEB UI.

# TechStack

1)UI testing in .net using Selenium.
2)API testing in .net using RestSharp.
3)Framework Used: BDD,NUnit
4)Tools:Postman,Visual Studio,Specflow(BDD Tool)
5)Programming Language : C#

# To Run Allure Report

Commands
-allure generate
-allure open
