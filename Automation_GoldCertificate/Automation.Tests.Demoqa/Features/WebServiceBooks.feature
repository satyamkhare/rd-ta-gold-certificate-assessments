﻿Feature: WebServiceBooks

A short summary of the feature

@APITesting(TC01)
Scenario: API Login Validation
	Given Make an Api call and create user login with username and password.
	Then Verify the status code and request body.

@UITesting(TC02)
Scenario: UI Login Validation
	Given :I entered username and password.
	And : Click the login button.
	Then :Validate Username on the Books Dashboard page.

@Testing(TC03)
Scenario: Validating Books
	Given : I make a get request to capture the books retrieved.
	Then : Validate Title, Author, Publisher with the UI url.

