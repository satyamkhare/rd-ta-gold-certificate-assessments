using Automation.Framework.Demoqa.API.APIUtils;
using Automation.Framework.Demoqa.API.Models;
using Automation.Framework.Demoqa.Common.CommonHelper;
using Automation.Framework.Demoqa.Common.CommonModels;
using Automation.Framework.Demoqa.UI.PageObjects.Actions;
using BoDi;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;
using TechTalk.SpecFlow;
using System.Text.Json;

namespace Automation.Tests.Demoqa.StepDefinitions
{
    [Binding]
    public class WebServiceBooksStepDefinitions :StepBase
    {
        private RestResponse response;
        private BooksListResponseBody apiBooks;
        private List<BooksResponseBody> uiBooks;
        private readonly BooksDashboardPageActions _booksDashboardPageActions;
        private readonly BookStorePageActions _bookStoreActions;
        private readonly LoginPageActions _loginPageActions;
        private AppSettings config = FileHelper.GiveFileData();

        protected WebServiceBooksStepDefinitions(IObjectContainer container) : base(container)
        {
            _booksDashboardPageActions = new BooksDashboardPageActions(Driver);
            _bookStoreActions= new BookStorePageActions(Driver);
            _loginPageActions = new LoginPageActions(Driver);
        }

        [Given(@"Make an Api call and create user login with username and password\.")]
        public void GivenMakeAnApiCallAndCreateUserLoginWithUsernameAndPassword_()
        {
            response = MethodFactoryApiUtils.Post(RestClient);
        }

        [Then(@"Verify the status code and request body\.")]
        public void ThenVerifyTheStatusCodeAndRequestBody_()
        {
            Assert.AreEqual(201, (int)response.StatusCode);
            var responseBody = JsonConvert.DeserializeObject<UserLoginPostResponseBody>(response.Content);
            if(responseBody != null)
            {
                Assert.AreEqual(config.username, responseBody.userName);
            }
        }

        [Given(@":I entered username and password\.")]
        public void GivenIEnteredUsernameAndPassword_()
        {
            _loginPageActions.EnterUsername(config.username);
            _loginPageActions.EnterPassword(config.password);
        }

        [Given(@": Click the login button\.")]
        public void GivenClickTheLoginButton_()
        {
           _loginPageActions.ClickLoginButton();
        }

        [Then(@":Validate Username on the Books Dashboard page\.")]
        public void ThenValidateUsernameOnTheBooksDashboardPage_()
        {
            string username = _booksDashboardPageActions.CheckUsername();
            Assert.AreEqual(config.username, username);
        }

        [Given(@": I make a get request to capture the books retrieved\.")]
        public void GivenIMakeAGetRequestToCaptureTheBooksRetrieved_()
        {
            response = MethodFactoryApiUtils.GetBooks(RestClient);
            apiBooks = JsonConvert.DeserializeObject<BooksListResponseBody>(response.Content);
            uiBooks = _bookStoreActions.retriveBooks();
           
        }

            [Then(@": Validate Title, Author, Publisher with the UI url\.")]
            public void ThenValidateTitleAuthorPublisherWithTheUIUrl_()
            {
                foreach (var apiBook in apiBooks.books)
                {
                    var matchingUiBook = uiBooks.FirstOrDefault(uiBook =>
                        uiBook.title == apiBook.title &&
                        uiBook.author == apiBook.author &&
                        uiBook.publisher == apiBook.publisher);
                    Assert.IsNotNull(matchingUiBook, $"Book '{apiBook.title}' was not found in the UI");
                }
            }
        
    }
}
