﻿using BoDi;
using OpenQA.Selenium;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Tests.Demoqa.StepDefinitions
{
    public class StepBase
    {
        [ThreadStatic]
        private readonly IObjectContainer Container;

        protected StepBase(IObjectContainer container)
        {
            Container = container;
        }

        protected IWebDriver Driver => Container.Resolve<IWebDriver>();
        protected RestClient RestClient => Container.Resolve<RestClient>();
    }
}
