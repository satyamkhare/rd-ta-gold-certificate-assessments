﻿using Automation.Framework.Demoqa.Common.CommonHelper;
using Automation.Framework.Demoqa.Common.CommonModels;
using Automation.Framework.Demoqa.UI.Factory;
using BoDi;
using Microsoft.VisualBasic;
using OpenQA.Selenium;
using RestSharp;
using System.Configuration;
using TechTalk.SpecFlow;

namespace Automation.Tests.Demoqa.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private IObjectContainer _container;
        private RestClient _restClient;
        private IWebDriver _driver;
        private AppSettings config = FileHelper.GiveFileData();
        public Hooks(IObjectContainer container)
        {
            _container = container;
        }

        [BeforeScenario]
        public void BeforeScenarioTC01()
        {
            _restClient = new RestClient(config.BaseUrl);
            _driver = DriverFactory.GetDriver(Framework.Demoqa.UI.Enums.BrowsersType.Chrome);
            _container.RegisterInstanceAs(_restClient);
            _container.RegisterInstanceAs(_driver);
        }
        [BeforeScenario("@UITesting(TC02)")]
        public void BeforeScenarioTC02()
        {
            _driver.Navigate().GoToUrl(config.LoginUrl);
            _driver.Manage().Window.Maximize();
            
        }
        [BeforeScenario("@Testing(TC03)")]
        public void BeforeScenarioTC03()
        {
            _driver.Navigate().GoToUrl(config.BooksUrl);
            _driver.Manage().Window.Maximize();  
        }

        [AfterScenario("@APITesting(TC01)")]
        public void AfterScenarioTC01()
        {
           _restClient.Dispose();
        }
        [AfterScenario("@UITesting(TC02)")]
        public void AfterScenarioTC02()
        {
            DriverFactory.QuitDriver();
        }
        [AfterScenario("@Testing(TC03)")]
        public void AfterScenarioTC03()
        {
            DriverFactory.QuitDriver();
        }
    }
}