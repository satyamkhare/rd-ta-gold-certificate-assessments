﻿using Automation.Framework.Demoqa.UI.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Safari;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.Factory
{
    public class DriverFactory
    {
        private static IWebDriver _driver;

        public static IWebDriver GetDriver(BrowsersType browserType)
        {
            if (_driver == null)
            {
                switch (browserType)
                {
                    case BrowsersType.Chrome:
                        _driver = new ChromeDriver();
                        break;

                    case BrowsersType.Safari:
                        _driver = new SafariDriver();
                        break;

                    case BrowsersType.Edge:
                        _driver = new EdgeDriver();
                        break;

                    default:
                        throw new ArgumentException("Unsupported Browser Type");
                }

            }

            return _driver;
        }
        public static void QuitDriver()
        {
            _driver?.Quit();
            _driver = null;
        }
    }
}
