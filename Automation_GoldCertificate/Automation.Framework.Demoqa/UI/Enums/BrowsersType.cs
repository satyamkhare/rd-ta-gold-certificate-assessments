﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.Enums
{
    public enum BrowsersType
    {
        Chrome,
        Safari,
        Edge
    }
}
