﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Locators
{
    public class BooksDashboardPageLocators
    {
        public static By _userNameText => By.Id("userName-value");
    }
}
