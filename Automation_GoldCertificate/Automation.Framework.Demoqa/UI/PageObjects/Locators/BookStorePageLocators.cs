﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Locators
{
    public class BookStorePageLocators
    {
        public static By _listOfRows => By.XPath("//div[@class='rt-tr-group']");
        public static By _listOfTitles => By.XPath(".//div[contains(@class, 'action-buttons')]/span/a");
        public static By _listOfAuthors => By.XPath(".//div[contains(@class, 'rt-td')][3]");
        public static By _listOfPublishers => By.XPath(".//div[contains(@class, 'rt-td')][4]");
    }
}
