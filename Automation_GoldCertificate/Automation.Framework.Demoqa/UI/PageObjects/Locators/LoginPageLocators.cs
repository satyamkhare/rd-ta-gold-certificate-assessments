﻿using OpenQA.Selenium;
using OpenQA.Selenium.DevTools.V121.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Locators
{
    public class LoginPageLocators
    {
        public static By _userNameInput => By.Id("userName");
        public static By _passwordInput => By.Id("password");
        public static By _loginButton => By.Id("login");

    }
}
