﻿using Automation.Framework.Demoqa.UI.PageObjects.Locators;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Actions
{
    public class BooksDashboardPageActions
    {
        private readonly IWebDriver _driver;

        public BooksDashboardPageActions(IWebDriver driver)
        {
            _driver = driver;
        }
        public string CheckUsername()
        {
            var result=_driver.FindElement(BooksDashboardPageLocators._userNameText).Text;
            return result;
        }
    }
}
