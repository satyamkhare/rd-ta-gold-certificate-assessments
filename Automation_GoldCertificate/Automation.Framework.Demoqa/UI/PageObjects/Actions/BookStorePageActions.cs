﻿using Automation.Framework.Demoqa.API.Models;
using Automation.Framework.Demoqa.UI.PageObjects.Locators;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Actions
{
    public class BookStorePageActions
    {
        
            private readonly IWebDriver _driver;

            public BookStorePageActions(IWebDriver driver)
            {
                _driver = driver;
            }

            public List<BooksResponseBody> retriveBooks()
            {
            List<BooksResponseBody> books = new List<BooksResponseBody>();

            
            var bookElements = _driver.FindElements(BookStorePageLocators._listOfRows);

            
            foreach (var bookElement in bookElements)
            {
                
                var author = bookElement.FindElement(BookStorePageLocators._listOfAuthors);
                var publisher = bookElement.FindElement(BookStorePageLocators._listOfPublishers);

                if( author != null && publisher!=null) 
                {
                   
                    string authorText = author.Text;
                    string publisherText = publisher.Text;
                    try
                    {
                        var title = bookElement.FindElement(BookStorePageLocators._listOfTitles);
                        string titleText= title.Text;
                        books.Add(new BooksResponseBody { title = titleText, author = authorText, publisher = publisherText });
                    }
                    catch(NoSuchElementException ex)
                    {
                        Console.WriteLine($"Message{ex.Message} and Stack Trace{ex.StackTrace}");
                    }
                }
            }
            return books;
        }

    }
}
