﻿using Automation.Framework.Demoqa.UI.PageObjects.Locators;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.PageObjects.Actions
{
    public class LoginPageActions
    {
        private readonly IWebDriver _driver;

        public LoginPageActions(IWebDriver driver)
        {
            _driver = driver;
        }
        public void EnterUsername(string username)
        {
            _driver.FindElement(LoginPageLocators._userNameInput).SendKeys(username);
        }
        public void EnterPassword(string password)
        {
            _driver.FindElement(LoginPageLocators._passwordInput).SendKeys(password);
        }
        public void ClickLoginButton()
        {
            _driver.FindElement(LoginPageLocators._loginButton).Click();
        }
    }
}
