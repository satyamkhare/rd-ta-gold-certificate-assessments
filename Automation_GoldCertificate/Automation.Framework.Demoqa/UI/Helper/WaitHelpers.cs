﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.UI.Helper
{
    public class WaitHelpers
    {
        private WebDriverWait wait;
        private DefaultWait<IWebDriver> fluentWait;
        public void ImplicitWait(IWebDriver driver, double timeout)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeout);
        }
        public IWebElement ExplicitWaitForElementExist(IWebDriver driver, TimeSpan timeout, By locator)
        {
            wait = new WebDriverWait(driver, timeout);
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
        }
        public IWebElement ExplicitWaitForElementToBeClickable(IWebDriver driver, TimeSpan timeout, By locator)
        {
            wait = new WebDriverWait(driver, timeout);
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
        }
        public IWebElement FluentWait(IWebDriver driver, double timeout, By locator)
        {
            fluentWait = new DefaultWait<IWebDriver>(driver);
            fluentWait.Timeout = TimeSpan.FromSeconds(timeout);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            return fluentWait.Until(x => x.FindElement(locator));
        }
    }
}
