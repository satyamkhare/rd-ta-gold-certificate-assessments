﻿using Automation.Framework.Demoqa.Common.CommonModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.Common.CommonHelper
{
    public class FileHelper
    {
        public static AppSettings ReadText(string path)
        {
            var text = File.ReadAllText(path);
            var appSettings = JsonConvert.DeserializeObject<AppSettings>(text);
            return appSettings;
        }

        public static string GiveFilePath(string fileName)
        {
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            string projectDirectory = Path.GetDirectoryName(assemblyLocation);

            while (!File.Exists(Path.Combine(projectDirectory, fileName)))
            {
                projectDirectory = Directory.GetParent(projectDirectory).FullName;
            }

            return Path.Combine(projectDirectory, fileName);
        }
        public static AppSettings GiveFileData()
        {
            var path = GiveFilePath("Application.json");
            var config = ReadText(path);
            return config;
        }
    }
}
