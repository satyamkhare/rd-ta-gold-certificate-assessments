﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.Common.CommonModels
{
    public class AppSettings
    {
        public string BaseUrl { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string PostResource { get; set; }
        public string LoginUrl { get; set; }
        public string GetResource { get; set; }
        public string BooksUrl { get; set; }
    }
}
