﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.Models
{
    public class BooksListResponseBody 
    {
        public List<BooksResponseBody> books { get; set; }
    }
}
