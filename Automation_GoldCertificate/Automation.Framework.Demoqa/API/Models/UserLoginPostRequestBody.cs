﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.Models
{
    public class UserLoginPostRequestBody
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
