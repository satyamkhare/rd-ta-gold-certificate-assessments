﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.APIUtils
{
    public class GenericApiUtils
    {
        public static RestResponse Get(RestClient client, string resource)
        {
            var response = client.Execute
                           (
                              RestRequestApiUtils.createRequest(resource, Method.Get)
                           );

            return response;
        }

        public static RestResponse Post(RestClient client, string resource, string payload)
        {
            var response = client.Execute
                           (
                              RestRequestApiUtils.createRequest(resource, Method.Post).AddJsonBody(payload)
                           );

            return response;
        }
    }
}
