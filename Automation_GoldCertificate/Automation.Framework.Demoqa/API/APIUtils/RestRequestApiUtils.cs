﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.APIUtils
{
    public class RestRequestApiUtils
    {
        private static RestRequest request;
        public static RestRequest createRequest(string resource, Method method)
        {
            request = new RestRequest(resource, method);
            return request;
        }
    }
}
