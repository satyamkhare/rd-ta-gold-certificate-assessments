﻿using Automation.Framework.Demoqa.Common.CommonHelper;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.APIUtils
{
    public class MethodFactoryApiUtils
    {
        public static RestResponse Post(RestClient client)
        {
            var config = FileHelper.GiveFileData();
            var payload = CreateRequestBodyUtils.CreateValidUserLoginPost();
            return GenericApiUtils.Post(client, config.PostResource, payload);
        }
        public static RestResponse GetBooks(RestClient client)
        {
            var config = FileHelper.GiveFileData();
            return GenericApiUtils.Get(client, config.GetResource);
        }
    }
}
