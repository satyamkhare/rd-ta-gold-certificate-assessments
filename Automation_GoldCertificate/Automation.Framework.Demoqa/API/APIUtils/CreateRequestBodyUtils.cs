﻿using Automation.Framework.Demoqa.API.Models;
using Automation.Framework.Demoqa.Common.CommonHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Framework.Demoqa.API.APIUtils
{
    public class CreateRequestBodyUtils
    {
        public static string CreateValidUserLoginPost()
        {
            var config = FileHelper.GiveFileData();
            UserLoginPostRequestBody userLoginPost = new UserLoginPostRequestBody()
            {
                userName = config.username,
                password = config.password,
            };
            return JsonConvert.SerializeObject(userLoginPost);
        }
    }
}
